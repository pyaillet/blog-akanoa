// .vuepress/config.js
module.exports = {
    title: 'La Forge', // Title for the site. This will be displayed in the navbar.
    theme: '@vuepress/theme-blog',
    themeConfig: {
      // Please keep looking down to see the available options.
    },
    dest: "public",
    base: "/",
    plugins: [
      ['container', {
        type: 'tip',
        defaultTitle: {
          '/': 'TIP',
          '/zh/': '提示'
        }
      }],
      ['container', {
        type: 'warning',
        defaultTitle: {
          '/': 'WARNING',
          '/zh/': '注意'
        }
      }],
      ['container', {
        type: 'danger',
        defaultTitle: {
          '/': 'DANGER',
          '/zh/': '警告'
        }
      }],
      ['container', {
        type: 'details',
        before: info => `<details class="custom-block details">${info ? `<summary>${info}</summary>` : ''}\n`,
        after: () => '</details>\n'
      }],
      ['container', {
        type: 'info',
        defaultTitle: "Information",
      }],
      // you can use this plugin multiple times
      'vuepress-plugin-mermaidjs',
      [
        '@akanoa/vuepress-plugin-goatcounter', {
          user: 'laforge'
        }
      ],
      [
          '@vssue/vuepress-plugin-vssue', {
              platform: 'gitlab',
              owner: 'Akanoa',
              repo: 'blog-comment',
              locale: 'fr',

              clientId: `${process.env.GITLAB_CLIENT_ID}`
          }
      ],
      [
        'vuepress-plugin-rss',
        {
          base_url: '/', // required
          site_url: 'https://lafor.ge', // required
          copyright: '2020 La Forge',
          // filter some post
          filter: (frontmatter) => { return [true|false] },
          // How many articles
          count: 20
        }
      ],
      ['vuepress-plugin-code-copy', {
        selector: 'div[class*="language-"]'
      }]
    ],
  }